# 卒業制作

## 環境構築方法

#### 1.WebStormのセットアップ
(1)インストール<br>　
 https://www.jetbrains.com/ja-jp/webstorm/ <br>
(2)

()Bootstrap-Vueの導入<br>
webstorm内のTerminalにて  **vue add bootstrap-vue**　を入力<br>
Use babel/polyfill(Y/N)は**N**で<br>
詳しくは政倉さんのスライドhttps://docs.google.com/presentation/d/1aXx67RUmv5Nbz0k5bmkIeIL_Z9RPGZ4MULEnLrt2xXA/edit#slide=id.g89909cd1ae_1_73
で確認<br><br>

()Fontawesomeの導入<br>
webstorm内のTerminalにて **vue add fontawesome** を入力<br>

####2.フレームワークのチュートリアル
・Vue.js: https://jp.vuejs.org/v2/guide/index.html <br>
・Bootstrap-vue: https://bootstrap-vue.org/ <br>
・Fontawesome: https://github.com/FortAwesome/vue-fontawesome
<br><br>

####vue-good-tableの使い方<br>
参考にしたサイトhttps://xaksis.github.io/vue-good-table/guide/<br>

()vue-good-tableの導入<br>
webstorm内のTerminalにて  **npm install --save vue-good-table**　を入力<br>
main.jsに以下を入力<br>
**import VueGoodTablePlugin from 'vue-good-table';**

**// import the styles**<br>
**import 'vue-good-table/dist/vue-good-table.css'**

**Vue.use(VueGoodTablePlugin);**<br>



