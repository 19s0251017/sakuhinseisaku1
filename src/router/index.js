import Vue from 'vue';
import VueRouter from 'vue-router';
// eslint-disable-next-line import/extensions
import Home from '../views/select-subject-grade';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/select-mode',
    name: 'Mode',
    component: () => import('../views/select-mode'),
  },
  {
    path: '/conditional',
    name: 'conditional',
    component: () => import('../views/conditional'),
  },
  {
    path: '/table1',
    name: 'table1',
    component: () => import('../views/table1'),
  },
  {
    path: '/table2',
    name: 'table2',
    component: () => import('../views/table2'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
